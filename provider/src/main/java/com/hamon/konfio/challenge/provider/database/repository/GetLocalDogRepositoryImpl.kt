package com.hamon.konfio.challenge.provider.database.repository

import com.hamon.konfio.challenge.provider.database.ObjectBox
import com.hamon.konfio.challenge.provider.models.GetDogsResponseItem

internal class GetLocalDogRepositoryImpl : GetLocalDogRepository {

    private val dogBox = ObjectBox.store.boxFor(GetDogsResponseItem::class.java)

    override fun addDogs(dogList: MutableList<GetDogsResponseItem>) {
        dogBox.put(dogList)
    }

    override fun getAllDogs(): MutableList<GetDogsResponseItem> = dogBox.all

    override fun removeAllDogs() {
        dogBox.removeAll()
    }
}

internal interface GetLocalDogRepository {
    fun addDogs(dogList: MutableList<GetDogsResponseItem>)
    fun getAllDogs(): MutableList<GetDogsResponseItem>
    fun removeAllDogs()
}