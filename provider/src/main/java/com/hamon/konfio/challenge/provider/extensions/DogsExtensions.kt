package com.hamon.konfio.challenge.provider.extensions

import com.hamon.konfio.challenge.provider.models.DogsUI
import com.hamon.konfio.challenge.provider.models.GetDogsResponseItem

fun GetDogsResponseItem.toDogUI(): DogsUI {
    return DogsUI(image = image, description = description, dogName = dogName, age = age)
}

fun MutableList<GetDogsResponseItem>.mapToDogUI(): MutableList<DogsUI> {
    return map { item -> item.toDogUI() }.toMutableList()
}