package com.hamon.konfio.challenge.provider.models

import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class GetDogsResponseItem(
    @Id var id: Long = 0,
    @field:SerializedName("image") val image: String? = null,
    @field:SerializedName("description") val description: String? = null,
    @field:SerializedName("dogName") val dogName: String? = null,
    @field:SerializedName("age") val age: Int? = null
)
