package com.hamon.konfio.challenge.provider.di

import com.hamon.konfio.challenge.provider.database.repository.GetLocalDogRepository
import com.hamon.konfio.challenge.provider.database.repository.GetLocalDogRepositoryImpl
import com.hamon.konfio.challenge.provider.network.API
import com.hamon.konfio.challenge.provider.network.services.GetDogsService
import com.hamon.konfio.challenge.provider.repositories.GetDogsRepository
import com.hamon.konfio.challenge.provider.repositories.GetDogsRepositoryImpl
import com.hamon.konfio.challenge.provider.useCases.GetDogsUseCase
import com.hamon.konfio.challenge.provider.useCases.GetDogsUsesCaseImpl
import org.koin.dsl.module

val providers = module {
    single { API.getServices<GetDogsService>() }
}

val databases = module {
    single<GetLocalDogRepository> { GetLocalDogRepositoryImpl() }
}

val repositories = module {
    single<GetDogsRepository> { GetDogsRepositoryImpl(get()) }
}

val useCases = module {
    single<GetDogsUseCase> { GetDogsUsesCaseImpl(get(), get()) }
}