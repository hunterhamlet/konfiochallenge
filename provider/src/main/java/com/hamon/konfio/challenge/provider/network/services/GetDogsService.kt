package com.hamon.konfio.challenge.provider.network.services

import com.hamon.konfio.challenge.provider.models.GetDogsResponseItem
import com.hamon.konfio.challenge.provider.network.GET_DOGS_SERVICE
import retrofit2.Response
import retrofit2.http.GET

interface GetDogsService {

    @GET(GET_DOGS_SERVICE)
    suspend fun getDogs(): Response<List<GetDogsResponseItem>>

}