package com.hamon.konfio.challenge.provider.database

import android.content.Context
import com.hamon.konfio.challenge.provider.models.MyObjectBox
import io.objectbox.BoxStore

object ObjectBox {
    lateinit var store: BoxStore
        private set

    fun init(context: Context) {
        store = MyObjectBox.builder().androidContext(context).build()
    }

}