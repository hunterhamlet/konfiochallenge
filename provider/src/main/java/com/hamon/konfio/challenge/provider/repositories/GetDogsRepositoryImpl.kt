package com.hamon.konfio.challenge.provider.repositories

import com.hamon.konfio.challenge.provider.actions.APIResponse
import com.hamon.konfio.challenge.provider.models.GetDogsResponseItem
import com.hamon.konfio.challenge.provider.network.services.GetDogsService

internal class GetDogsRepositoryImpl(private val getDogsService: GetDogsService) :
    GetDogsRepository {

    override suspend fun getDogs(): APIResponse {
        return try {
            val response = getDogsService.getDogs()
            if (response.isSuccessful) {
                APIResponse.Success(data = response.body()?.toMutableList() ?: run {
                    mutableListOf()
                })
            } else {
                APIResponse.Success(data = mutableListOf())
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
            APIResponse.Error(message = exception.message)
        }
    }

}

internal interface GetDogsRepository {
    suspend fun getDogs(): APIResponse
}