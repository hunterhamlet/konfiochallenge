package com.hamon.konfio.challenge.provider.useCases

import com.hamon.konfio.challenge.provider.actions.APIResponse
import com.hamon.konfio.challenge.provider.database.repository.GetLocalDogRepository
import com.hamon.konfio.challenge.provider.extensions.mapToDogUI
import com.hamon.konfio.challenge.provider.models.GetDogsResponseItem
import com.hamon.konfio.challenge.provider.repositories.GetDogsRepository

internal class GetDogsUsesCaseImpl(
    private val getDogsRepository: GetDogsRepository,
    private val getLocalDogRepository: GetLocalDogRepository
) :
    GetDogsUseCase {
    override suspend operator fun invoke(hasNetworkConnection: Boolean): APIResponse {
        return if (hasNetworkConnection) {
            handleApiResponse(getDogsRepository.getDogs())
        } else {
            APIResponse.Success(data = getLocalDogRepository.getAllDogs().mapToDogUI())
        }
    }
}

interface GetDogsUseCase {
    suspend operator fun invoke(hasNetworkConnection: Boolean = false): APIResponse
}

private fun handleApiResponse(response: APIResponse): APIResponse {
    return when (response) {
        is APIResponse.Success<*> -> APIResponse.Success(data = (response.data as MutableList<GetDogsResponseItem>).mapToDogUI())
        else -> response
    }
}