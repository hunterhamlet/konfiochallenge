package com.hamon.konfio.challenge.provider.models

data class DogsUI(
    val image: String? = null,
    val description: String? = null,
    val dogName: String? = null,
    val age: Int? = null
)