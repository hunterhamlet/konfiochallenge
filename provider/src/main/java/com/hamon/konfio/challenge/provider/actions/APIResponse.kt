package com.hamon.konfio.challenge.provider.actions

sealed class APIResponse {
    data class Success<out T>(val data: T) : APIResponse()
    data class Error(val message: String?) : APIResponse()
}