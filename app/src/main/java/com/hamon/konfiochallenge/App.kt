package com.hamon.konfiochallenge

import androidx.multidex.MultiDexApplication
import com.hamon.konfio.challenge.provider.database.ObjectBox
import com.hamon.konfio.challenge.provider.di.databases
import com.hamon.konfio.challenge.provider.di.providers
import com.hamon.konfio.challenge.provider.di.repositories
import com.hamon.konfio.challenge.provider.di.useCases
import com.hamon.konfiochallenge.di.viewModels
import com.hamon.konfiochallenge.utils.CoilLoader
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            androidFileProperties()
            koin.apply {
                loadModules(
                    arrayListOf(
                        providers,
                        databases,
                        repositories,
                        useCases,
                        viewModels
                    )
                )
                createRootScope()
            }
        }
        ObjectBox.init(this)
        CoilLoader.init(this)
    }

}