package com.hamon.konfiochallenge.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hamon.konfio.challenge.provider.actions.APIResponse
import com.hamon.konfio.challenge.provider.models.DogsUI
import com.hamon.konfio.challenge.provider.useCases.GetDogsUseCase
import com.hamon.konfiochallenge.actions.ResponseUI
import com.hamon.konfiochallenge.extensions.checkInternetConnection
import kotlinx.coroutines.launch

class MainViewModel(
    private val app: Application,
    private val getDogsUseCase: GetDogsUseCase
) : AndroidViewModel(app) {

    private val _responseUIObservable: MutableLiveData<ResponseUI> by lazy {
        MutableLiveData<ResponseUI>()
    }
    val responseUIObservable: LiveData<ResponseUI> get() = _responseUIObservable

    fun getDogs() {
        app.checkInternetConnection { hasInternetConnection ->
            _responseUIObservable.value = ResponseUI.Loading
            viewModelScope.launch {
                handleResponse(getDogsUseCase(hasInternetConnection))
            }
        }
    }

    private fun handleResponse(apiResponse: APIResponse) {
        when (apiResponse) {
            is APIResponse.Success<*> -> handleSuccess(apiResponse)
            is APIResponse.Error -> handleErrorResponse(apiResponse)
        }
    }

    private fun handleSuccess(successResponse: APIResponse.Success<*>) {
        val dogList = (successResponse.data as MutableList<DogsUI>)
        _responseUIObservable.value = if (dogList.isEmpty().not()) {
            ResponseUI.Success(data = dogList)
        } else {
            ResponseUI.EmptyState
        }
    }

    private fun handleErrorResponse(errorResponse: APIResponse.Error) {
        _responseUIObservable.value = ResponseUI.Error(message = errorResponse.message)
    }

}