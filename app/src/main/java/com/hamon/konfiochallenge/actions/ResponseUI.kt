package com.hamon.konfiochallenge.actions

sealed class ResponseUI {
    object Init : ResponseUI()
    object Loading : ResponseUI()
    object EmptyState : ResponseUI()
    data class Success<T>(val data: T) : ResponseUI()
    data class Error(val message: String?) : ResponseUI()
}