package com.hamon.konfiochallenge.extensions

import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

fun Fragment.handleSnackBarError(msg: String) {
    this.view?.let {
        Snackbar.make(requireContext(), it, msg, Snackbar.LENGTH_SHORT)
            .setBackgroundTint(requireContext().getColor(android.R.color.holo_red_dark))
            .setTextColor(requireContext().getColor(android.R.color.white))
            .show()
    }
}