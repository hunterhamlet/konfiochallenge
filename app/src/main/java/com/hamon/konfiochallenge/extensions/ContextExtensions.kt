package com.hamon.konfiochallenge.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import androidx.annotation.StringRes
import androidx.viewbinding.ViewBinding

fun Context.checkConnectivityInAndroidPOrBelow(): Boolean {
    return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo?.isConnectedOrConnecting
        ?: false
}

fun Context.checkConnectivityInAndroidPOrHigher(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork) != null
}

fun Context.checkNetworkState(): Boolean {
    return if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
        checkConnectivityInAndroidPOrBelow()
    } else {
        checkConnectivityInAndroidPOrHigher()
    }
}

fun Context.checkInternetConnection(hasInternetConnection: (Boolean) -> Unit) {
    val command = "ping -c 1 google.com"
    return try {
        if (checkNetworkState()) {
            hasInternetConnection.invoke(Runtime.getRuntime().exec(command).waitFor() == 0)
        } else {
            hasInternetConnection.invoke(false)
        }
    } catch (exception: Exception) {
        exception.printStackTrace()
        hasInternetConnection.invoke(false)
    }
}
