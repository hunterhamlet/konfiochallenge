package com.hamon.konfiochallenge.ui.viewHolder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.RoundedCornersTransformation
import com.hamon.konfio.challenge.provider.models.DogsUI
import com.hamon.konfiochallenge.R
import com.hamon.konfiochallenge.databinding.ItemCardDogBinding
import com.hamon.konfiochallenge.utils.CoilLoader
import com.hamon.konfiochallenge.style.R as StyleResources

class DogAdapter(private val dogList: MutableList<DogsUI> = mutableListOf()) :
    RecyclerView.Adapter<DogAdapter.ViewHolder>() {

    fun submitList(newDogList: MutableList<DogsUI>) {
        dogList.clear()
        dogList.addAll(newDogList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemCardDogBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dogList[position])
    }

    override fun getItemCount(): Int = dogList.size

    inner class ViewHolder(private val binding: ItemCardDogBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: DogsUI) {
            binding.apply {
                ivDog.load(item.image, CoilLoader.getImageLoader()) {
                    crossfade(true)
                    transformations(
                        RoundedCornersTransformation(
                            radius = binding.root.context.resources.getDimension(StyleResources.dimen.card_radius_2)
                        )
                    )
                }
                tvDogName.text = item.dogName
                tvDogDescription.text = item.description
                tvDogYear.text =
                    if (item.age != 1) binding.root.context.getString(
                        R.string.main_almost_years,
                        item.age.toString()
                    ) else binding.root.context.getString(
                        R.string.main_almost_year,
                        item.age.toString()
                    )
            }
        }

    }

}