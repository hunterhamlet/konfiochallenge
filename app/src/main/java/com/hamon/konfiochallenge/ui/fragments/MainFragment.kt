package com.hamon.konfiochallenge.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hamon.konfio.challenge.provider.models.DogsUI
import com.hamon.konfiochallenge.R
import com.hamon.konfiochallenge.actions.ResponseUI
import com.hamon.konfiochallenge.databinding.FragmentMainBinding
import com.hamon.konfiochallenge.extensions.handleSnackBarError
import com.hamon.konfiochallenge.extensions.hide
import com.hamon.konfiochallenge.extensions.show
import com.hamon.konfiochallenge.ui.viewHolder.DogAdapter
import com.hamon.konfiochallenge.viewModels.MainViewModel
import org.koin.android.ext.android.inject

class MainFragment : Fragment() {

    private val binding by lazy {
        FragmentMainBinding.inflate(layoutInflater)
    }
    private val viewModel by inject<MainViewModel>()
    private val adapter by lazy { DogAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        viewModel.getDogs()
        setupObservable()
    }

    private fun setupRecyclerView() {
        binding.rvDogs.adapter = adapter
    }

    private fun setupObservable() {
        viewModel.responseUIObservable.observe(viewLifecycleOwner) {
            when (it) {
                is ResponseUI.Success<*> -> handleSuccessResponse(it)
                is ResponseUI.Error -> handleErrorResponse(it.message)
                is ResponseUI.Loading -> showLoading()
            }
        }
    }

    private fun handleSuccessResponse(responseUI: ResponseUI.Success<*>) {
        adapter.submitList(responseUI.data as MutableList<DogsUI>)
        hideLoading()
    }

    private fun handleErrorResponse(message: String?) {
        handleSnackBarError(message ?: getString(R.string.mian_error_default))
    }

    private fun showLoading() {
        binding.apply {
            rvDogs.hide()
            loadingView.root.show()
        }
    }

    private fun hideLoading() {
        binding.apply {
            loadingView.root.hide()
            rvDogs.show()
        }
    }


}