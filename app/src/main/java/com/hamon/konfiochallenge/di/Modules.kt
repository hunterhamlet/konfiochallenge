package com.hamon.konfiochallenge.di

import com.hamon.konfiochallenge.viewModels.MainViewModel
import org.koin.dsl.module

val viewModels = module {
    single { MainViewModel(get(), get()) }
}