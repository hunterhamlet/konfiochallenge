package com.hamon.konfiochallenge.utils

import android.content.Context
import coil.ImageLoader
import coil.request.CachePolicy

object CoilLoader {

    private lateinit var imageLoader: ImageLoader

    fun init(context: Context) {
        imageLoader = ImageLoader.Builder(context)
            .memoryCachePolicy(CachePolicy.ENABLED)
            .build()
    }

    fun getImageLoader() = imageLoader

}